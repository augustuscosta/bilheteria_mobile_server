class CreateConteudos < ActiveRecord::Migration
  def change
    create_table :conteudos do |t|
      t.string :titulo
      t.string :subtitulo
      t.string :categoria
      t.text :descricao
      t.string :endereco
      t.float :latitude
      t.float :longitude
      t.boolean :ativo
      t.integer :parent_id
      t.integer :empresa_id

      t.timestamps
    end
  end
end

class AddC2dmToEmpresa < ActiveRecord::Migration
    def self.up
      add_column :empresas, :c2dm_email,              :string
      add_column :empresas, :c2dm_password,           :string
      add_column :empresas, :c2dm_registration_id,    :string
    end

    def self.down
      remove_column :empresas, :c2dm_email
      remove_column :empresas, :c2dm_password
      remove_column :empresas, :c2dm_registration_id
    end
end

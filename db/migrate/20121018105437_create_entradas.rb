class CreateEntradas < ActiveRecord::Migration
  def change
    create_table :entradas do |t|
      t.datetime :data
      t.integer :quantidade
      t.integer :tipo_de_ingresso_id

      t.timestamps
    end
  end
end

class AddUrlToConteudo < ActiveRecord::Migration
  def self.up
      add_column :conteudos, :url,    :string
    end

    def self.down
      remove_column :conteudos, :url
    end
end

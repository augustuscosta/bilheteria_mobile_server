class CreateIngressos < ActiveRecord::Migration
  def change
    create_table :ingressos do |t|
      t.integer :usuario_id
      t.integer :conteudo_id
      t.integer :tipo_de_ingresso_id
      t.string  :transacao
      t.text  :resposta
      t.string :transacao_status

      t.timestamps
    end
  end
end

class CreateTipoDeIngressos < ActiveRecord::Migration
  def change
    create_table :tipo_de_ingressos do |t|
      t.string :titulo
      t.string :descricao
      t.decimal :valor
      t.integer :quantidade_ingressos
      t.integer :empresa_id

      t.timestamps
    end
  end
end

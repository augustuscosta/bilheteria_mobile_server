class AddEnderecoCepCidadeEstadoNascimentoToUsuarios < ActiveRecord::Migration
  def change
    add_column :usuarios, :endereco, :string

    add_column :usuarios, :cep, :string

    add_column :usuarios, :cidade, :string

    add_column :usuarios, :estado, :string

    add_column :usuarios, :nascimento, :date

  end
end

class AddConteudosTipoDeIngressosJoinTable < ActiveRecord::Migration
  def up
  	create_table :conteudos_tipo_de_ingressos, :id => false do |t|
      t.integer :conteudo_id
      t.integer :tipo_de_ingresso_id
    end
  end

  def down
  	drop_table :conteudos_tipo_de_ingressos
  end
end

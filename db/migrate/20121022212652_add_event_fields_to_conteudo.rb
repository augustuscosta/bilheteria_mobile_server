class AddEventFieldsToConteudo < ActiveRecord::Migration
  def change
    add_column :conteudos, :data, :datetime

    add_column :conteudos, :data_incio_vendas, :datetime

    add_column :conteudos, :data_fim_vendas, :datetime

  end
end

class AddFacebookAndTwitterToEmpresa < ActiveRecord::Migration
  def change
    add_column :empresas, :facebook, :string

    add_column :empresas, :twitter, :string

  end
end

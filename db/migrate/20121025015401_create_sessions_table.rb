class CreateSessionsTable < ActiveRecord::Migration
  def up
        create_table :sessions do |t|
                t.timestamps
        end
  end

  def down
  end
end
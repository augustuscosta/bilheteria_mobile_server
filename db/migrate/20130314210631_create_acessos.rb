class CreateAcessos < ActiveRecord::Migration
  def change
    create_table :acessos do |t|
      t.integer :ingresso_id
      t.integer :entrada_id
      t.datetime :data
      t.integer :sent

      t.timestamps
    end
  end
end

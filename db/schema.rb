# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130715225119) do

  create_table "acessos", :force => true do |t|
    t.integer  "ingresso_id"
    t.integer  "entrada_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "conteudos", :force => true do |t|
    t.string   "titulo"
    t.string   "subtitulo"
    t.string   "categoria"
    t.text     "descricao"
    t.string   "endereco"
    t.float    "latitude"
    t.float    "longitude"
    t.boolean  "ativo"
    t.integer  "parent_id"
    t.integer  "empresa_id"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.boolean  "social"
    t.boolean  "destaque"
    t.string   "url",                     :limit => nil
    t.string   "row_imagem_file_name"
    t.string   "row_imagem_content_type"
    t.integer  "row_imagem_file_size"
    t.datetime "row_imagem_updated_at"
    t.datetime "data"
    t.datetime "data_incio_vendas"
    t.datetime "data_fim_vendas"
  end

  create_table "conteudos_tipo_de_ingressos", :id => false, :force => true do |t|
    t.integer "conteudo_id"
    t.integer "tipo_de_ingresso_id"
  end

  create_table "empresas", :force => true do |t|
    t.string   "nome"
    t.string   "cnpj"
    t.string   "telefone"
    t.string   "email"
    t.string   "identificador"
    t.string   "token"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.string   "c2dm_email",           :limit => nil
    t.string   "c2dm_password",        :limit => nil
    t.string   "c2dm_registration_id", :limit => nil
    t.string   "facebook"
    t.string   "twitter"
  end

  create_table "entradas", :force => true do |t|
    t.datetime "data"
    t.integer  "quantidade"
    t.integer  "tipo_de_ingresso_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "ingressos", :force => true do |t|
    t.integer  "usuario_id"
    t.integer  "conteudo_id"
    t.integer  "tipo_de_ingresso_id"
    t.string   "transacao"
    t.string   "resposta"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "ingressos_dashboards", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "sessions", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "tipo_de_ingressos", :force => true do |t|
    t.string   "titulo"
    t.string   "descricao"
    t.decimal  "valor"
    t.integer  "quantidade_ingressos"
    t.integer  "empresa_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "usuarios", :force => true do |t|
    t.string   "nome"
    t.string   "cpf"
    t.string   "rg"
    t.string   "email"
    t.string   "password"
    t.string   "password_digest"
    t.string   "telefone"
    t.string   "tipo"
    t.integer  "empresa_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "endereco"
    t.string   "cep"
    t.string   "cidade"
    t.string   "estado"
    t.date     "nascimento"
  end

end

class Ingresso < ActiveRecord::Base
	self.include_root_in_json = true
	belongs_to  :usuario
	belongs_to  :conteudo
	belongs_to  :tipo_de_ingresso

	has_many :acessos
end
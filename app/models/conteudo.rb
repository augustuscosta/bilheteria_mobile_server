class Conteudo < ActiveRecord::Base
  self.include_root_in_json = true
  belongs_to  :empresa, :foreign_key => "empresa_id", :class_name => "Empresa"
  belongs_to  :conteudo, :class_name => "Conteudo", :foreign_key => "parent_id"
  has_many    :ingressos, dependent: :destroy, :foreign_key => "conteudo_id", :class_name => "Ingresso"
  has_many    :counteudos, dependent: :destroy, :class_name => "Conteudo", :foreign_key => "parent_id"
  has_and_belongs_to_many :tipo_de_ingressos

  acts_as_tree :order => "id"
  
  #after_create :send_notification
  
  
  
  has_attached_file :imagem,
      :styles => {
      :thumb  => "100x100",
      :medium => "200x200",
      :large => "600x400",
      :icon => "42x42"
  },
    :storage => :s3,
    :s3_credentials => "#{Rails.root}/config/s3.yml",
    :path => ":attachment/:id/:style.:extension",
    :bucket => 'skynetimagens'

  has_attached_file :row_imagem,
      :styles => {
      :thumb  => "100x100",
      :medium => "200x200",
      :large => "600x400"
  },
    :storage => :s3,
    :s3_credentials => "#{Rails.root}/config/s3.yml",
    :path => ":attachment/:id/:style.:extension",
    :bucket => 'skynetimagens'
    
    
  def send_notification
    unless self.destaque == false || self.ativo == false
      #Apple notification
      unless self.empresa.token == nil
        APN.notify(self.empresa.token, :alert => self.titulo, :badge => 4, :sound => true, :other_information => self.subtitulo)
      end
       #Android c2dm
      unless self.empresa.c2dm_email == nil || self.empresa.c2dm_password == nil || self.empresa.c2dm_registration_id == nil
        SpeedyC2DM::API.set_account(self.empresa.c2dm_email, self.empresa.c2dm_password)
        options = {
          :registration_id => self.empresa.c2dm_registration_id,
          :message => self.titulo,
          :extra_data => self.id,
          :collapse_key => "some-collapse-key" #verificar o que é collapse-key
        }
        response = SpeedyC2DM::API.send_notification(options)
      end
    end
  end
  
  def as_json(options={})
     super(:include => :tipo_de_ingressos, :methods => [:imagem_url, :imagem_thumb_url,:imagem_medium_url,:imagem_large_url,:imagem_icon_url,:row_imagem_url, :row_imagem_thumb_url,:row_imagem_medium_url,:row_imagem_large_url])
  end
    
    def imagem_url
        if imagem_updated_at != nil
          imagem.url
        end
    end

    def imagem_thumb_url
      if imagem_updated_at != nil
        imagem.url(:thumb)
      end
    end

    def imagem_medium_url
      if imagem_updated_at != nil
        imagem.url(:medium)
      end
    end

    def imagem_large_url
      if imagem_updated_at != nil
        imagem.url(:large)
      end
    end

    def imagem_icon_url
      if imagem_updated_at != nil
        imagem.url(:icon)
      end
    end

    def row_imagem_url
        if row_imagem_updated_at != nil
          row_imagem.url
        end
    end

    def row_imagem_thumb_url
      if row_imagem_updated_at != nil
        row_imagem.url(:thumb)
      end
    end

    def row_imagem_medium_url
      if row_imagem_updated_at != nil
        row_imagem.url(:medium)
      end
    end

    def row_imagem_large_url
      if row_imagem_updated_at != nil
        row_imagem.url(:large)
      end
    end 
end

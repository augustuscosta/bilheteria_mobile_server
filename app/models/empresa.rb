class Empresa < ActiveRecord::Base
  
  has_many    :conteudos, dependent: :destroy, :foreign_key => "empresa_id", :class_name => "Conteudo"
  has_many    :usuarios, dependent: :destroy, :foreign_key => "empresa_id", :class_name => "Usuario"
  has_many    :tipo_de_ingressos, dependent: :destroy, :foreign_key => "empresa_id", :class_name => "TipoDeIngresso"
  validates_presence_of :identificador
  validates_uniqueness_of :identificador
  
  
end

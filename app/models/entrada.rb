class Entrada < ActiveRecord::Base
	belongs_to  :tipo_de_ingresso

	has_many :acessos
end

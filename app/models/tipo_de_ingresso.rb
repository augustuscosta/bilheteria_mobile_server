class TipoDeIngresso < ActiveRecord::Base
	belongs_to  :empresa
	has_many    :ingressos, dependent: :destroy, :foreign_key => "tipo_de_ingresso_id", :class_name => "Ingresso"
	has_many    :entradas, dependent: :destroy, :foreign_key => "tipo_de_ingresso_id", :class_name => "Entrada"
	has_and_belongs_to_many :conteudos

	accepts_nested_attributes_for :entradas, :allow_destroy => true


	def as_json(options=nil)
     super(:include => :entradas)
  	end

end

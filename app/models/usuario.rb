class Usuario < ActiveRecord::Base
	self.include_root_in_json = true
	belongs_to :empresa
	belongs_to :ingresso
  	has_many    :ingressos, dependent: :destroy, :foreign_key => "usuario_id", :class_name => "Ingresso"
	validates_uniqueness_of :email
	validates_presence_of 	:email
	validates_uniqueness_of :cpf
	validates_presence_of 	:cpf
  	has_secure_password
  	validates_presence_of :password, :on => :create

	attr_accessor :password_confirmation

	

end

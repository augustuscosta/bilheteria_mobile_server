class TipoDeIngressosController < ApplicationController
  before_filter :get_empresa, :get_conteudos, :checar_permissoes_empresa_dependente

  def get_empresa
    @empresa = Empresa.find(params[:empresa_id])
  end

  def get_conteudos
    @conteudos = @empresa.conteudos
  end

  def index
    @tipo_de_ingressos = @empresa.tipo_de_ingressos

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tipo_de_ingressos.to_json(:include => :entradas) }
    end
  end

  def show
    @tipo_de_ingresso = @empresa.tipo_de_ingressos.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: [@empresa,@tipo_de_ingresso] }
    end
  end

  def new
    @tipo_de_ingresso = TipoDeIngresso.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: [@empresa,@tipo_de_ingresso] }
    end
  end

  def edit
    @tipo_de_ingresso = @empresa.tipo_de_ingressos.find(params[:id])
  end

  def create
    @tipo_de_ingresso = @empresa.tipo_de_ingressos.new(params[:tipo_de_ingresso])

    respond_to do |format|
      if @tipo_de_ingresso.save
        format.html { redirect_to [@empresa,@tipo_de_ingresso], notice: 'Tipo de ingresso was successfully created.' }
        format.json { render json: [@empresa,@tipo_de_ingresso], status: :created, location: [@empresa,@tipo_de_ingresso] }
      else
        format.html { render action: "new" }
        format.json { render json: @tipo_de_ingresso.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /empresas/1
  # PUT /empresas/1.json
  def update
    @tipo_de_ingresso = @empresa.tipo_de_ingressos.find(params[:id])

    respond_to do |format|
      if @tipo_de_ingresso.update_attributes(params[:tipo_de_ingresso])
        format.html { redirect_to [@empresa,@tipo_de_ingresso], notice: 'tipo_de_ingresso was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @tipo_de_ingresso.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @tipo_de_ingresso = @empresa.tipo_de_ingressos.find(params[:id])
    @tipo_de_ingresso.destroy

    respond_to do |format|
      format.html { redirect_to [@empresa,@tipo_de_ingresso] }
      format.json { head :no_content }
    end
  end
end

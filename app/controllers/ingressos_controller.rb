class IngressosController < ApplicationController

#!/usr/bin/env ruby
require 'openssl'
require 'base64'

  
  # GET /ingressos
  # GET /ingressos.json
  def index
    usuario = Usuario.find_by_id(session[:usuario_id])

    logger.info "Estou acessando ingressos"

    if usuario.tipo == 'administrador'
      @ingressos = Ingresso.all
    else
      if usuario.tipo == 'contratante'
        @ingressos = usuario.empresa.ingressos
      else
        logger.info "CONTRATANTE2"
        @ingressos = usuario.ingressos
      end
    end

    tipo_de_ingresso = :tipo_de_ingresso

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @ingressos.to_json(:include => [:usuario, :acessos, :tipo_de_ingresso => { :include => :entradas }]) }
    end
  end

  # GET /ingressos/1
  # GET /ingressos/1.json
  def show
    usuario = Usuario.find_by_id(session[:usuario_id])

    if usuario.tipo == 'administrador'
      @conteudo = Conteudo.find(params[:id])
    else
      if usuario.tipo == 'contratante'
        @conteudo = usuario.empresa.conteudos.find(params[:id])
      else
        @conteudo = usuario.conteudos.find(params[:id])
      end
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @conteudo.ingressos.to_json(:except => :resposta) }
    end
  end

  # GET /ingressos/new
  # GET /ingressos/new.json
  def new
    @ingresso = Ingresso.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @ingresso }
    end
  end

  # GET /ingressos/1/edit
  def edit
    @ingresso = Ingresso.find(params[:id])
  end

  # POST /ingressos
  # POST /ingressos.json
  def create
    tipo_de_ingresso = TipoDeIngresso.find(params[:ingresso]['tipo_de_ingresso_id'])
    if tipo_de_ingresso.ingressos == nil or tipo_de_ingresso.ingressos.where("resposta IS NOT NULL").count < tipo_de_ingresso.quantidade_ingressos
      params[:ingresso][:usuario_id] = session[:usuario_id]
      @ingresso = Ingresso.new(params[:ingresso])
    end
    respond_to do |format|
      if @ingresso
        if @ingresso.save
          format.html { redirect_to @ingresso, notice: 'Ingresso was successfully created.' }
          format.json { render json: @ingresso, status: :created, location: @ingresso }
        else
          format.json { render json: @ingresso.errors, status: :unprocessable_entity }
        end
      else
        format.json { render :json => [], status: :unprocessable_entity }
      end
    end
  end

  # PUT /ingressos/1
  # PUT /ingressos/1.json
  def update
    @ingresso = Ingresso.find(params[:id])
    create_response_string(@ingresso)

     message = @ingresso.to_json(:except => [:tipo_de_ingresso_id])

     logger.info message

    if (params[:ingresso][:transacao] != nil && (params[:ingresso][:resposta] == "" || params[:ingresso][:resposta] == nil))
      
      public_key_file = 'public.pem';

      public_key = OpenSSL::PKey::RSA.new(File.read(public_key_file))

      private_key_file = 'private.pem';

      password = "1q2w3e"

      private_key = OpenSSL::PKey::RSA.new(File.read(private_key_file),password)

      # For testing purposes only!
      message = @ingresso.to_json(:except => [:resposta]);

      logger.info message + " <-RESPOSTAAA"

      cipher = OpenSSL::Cipher::Cipher.new('aes-256-cbc')
      cipher.encrypt # Call this before setting key or iv

      cipher.key = random_key = cipher.random_key
      cipher.iv = random_iv = "OTGcipherivkeyOTG"

      logger.info random_key + '<- Tamanho da chave'

      encrypted_key = Base64.encode64(public_key.public_encrypt(random_key))

      # Encrypt plaintext using Triple DES
      
      ciphertext = cipher.update(message)
      ciphertext << cipher.final



      # Base64-encode the ciphertext
      encodedCipherText = Base64.encode64(ciphertext)

      toSend = "#OTG1#" + encrypted_key + "#OT1#" + encodedCipherText

      params[:ingresso][:resposta] = toSend

      logger.info "Texto encriptado ->" + toSend + "  Com a chave ->" + random_key

      xipher = OpenSSL::Cipher::Cipher.new('aes-256-cbc')
      # Base64-decode the ciphertext and decrypt it
      xipher.decrypt
      xipher.key = private_key.private_decrypt(Base64.decode64(encrypted_key))
      xipher.iv = random_iv
      plaintext = xipher.update(Base64.decode64(encodedCipherText))
      plaintext << xipher.final

      logger.info "Texto desencriptado ->" +  plaintext + "  Com a chave ->" + random_key

    end
    respond_to do |format|
      if @ingresso.update_attributes(params[:ingresso])
        format.html { redirect_to @ingresso, notice: 'Ingresso was successfully updated.' }
        format.json { head :no_content }
      else
        format.json { render json: @ingresso.errors, status: :unprocessable_entity }
      end
    end
  end

  def create_response_string(ingresso)
    json = ingresso.to_json
    logger.info json 
    json
  end

  def destroy
    @ingresso = Ingresso.find(params[:id])
    @ingresso.destroy

    respond_to do |format|
      format.html { redirect_to ingressos_url }
      format.json { head :no_content }
    end
  end
  
  def get_ingressos_by_conteudo
    @conteudo = Conteudo.find_by_id(params[:conteudo_id])
    
    @ingressos = @conteudo.ingressos
    
    respond_to do |format|
      format.json { render json: @ingressos.to_json(:include => [:usuario, :tipo_de_ingresso => { :include => :entradas }]) }
    end
  end

end

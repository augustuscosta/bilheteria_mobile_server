class ApplicationController < ActionController::Base
  protect_from_forgery
  force_ssl
  skip_before_filter  :verify_authenticity_token
  helper_method :current_user

  def authorize
      if session[:usuario_id].nil?
        render :file => "#{Rails.public_path}/404.html", :status => :unauthorized
      end

  end

  def current_user
    @current_user ||= Usuario.find(session[:usuario_id]) if session[:usuario_id]
  end
  
  def check_role(empresa)

    if session[:usuario_id]
      user = Usuario.find_by_id(session[:usuario_id])
      if user.tipo != 'administrador'
        if user.empresa.id != empresa.id
          render :file => "#{Rails.public_path}/404.html", :status => :unauthorized
        end
      end
    end

  end

  def checar_permissoes_empresa

    user = Usuario.find_by_id(session[:usuario_id])
    if user.tipo == 'usuario'
      render :file => "#{Rails.public_path}/404.html", :status => :unauthorized
     end

    if params[:id]

      empresa = Empresa.find_by_id(params[:id])
      if empresa
        check_role(empresa)
      end
      if user == nil
        redirect_to new_session_path
      end
    else
      if user.tipo != 'administrador'
        render :file => "#{Rails.public_path}/404.html", :status => :unauthorized
     end
   end
  end

  def checar_permissoes
    user = Usuario.find_by_id(session[:usuario_id])

    if user == nil
        redirect_to new_session_path
      else
        if user.tipo == 'usuario'
          #render :file => "#{Rails.public_path}/404.html", :status => :unauthorized
        end
      end
  end

  def checar_permissoes_empresa_dependente
    if params[:empresa_id]
      empresa = Empresa.find_by_id(params[:empresa_id])
      if empresa
        check_role(empresa)
      end
    end
  end

end

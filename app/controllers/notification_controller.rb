class NotificationController < ApplicationController
require "net/http"
require "uri"
require 'rest_client'
require 'nokogiri'
require 'base64'

    def index
        render :file => "#{Rails.public_path}/404.html"      
    end

    def create
        transaction = PagSeguro::Transaction.find_by_code(params[:notificationCode])
        if transaction.errors.empty?  
            logger.info "A transacao chegou e esta ok"
            logger.info "Status:"
            logger.info transaction.status.status
            logger.info "Codigo:"
            logger.info transaction.code

            status = transaction.status.status.to_s


            if (status == "paid")
                logger.info "Entrou como paga"
                transaction.items.each do |item|
                    unless item.id.to_i == 0
                        ingresso = Ingresso.find(item.id.to_i)
                        resposta = Base64.encode64(ingresso.to_json(:except => [:resposta, :created_at, :updated_at]))
                        ingresso.update_attributes(:transacao => transaction.code, :resposta => resposta, :transacao_status => status)
                    end
                end
                render nothing: true, status: 200
                return
            else
                if status == "cancelled"
                    render nothing: true, status: 200
                    return
                end
            else
                logger.info "Entrou como nao paga"
                transaction.items.each do |item|
                    unless item.id.to_i == 0
                        ingresso = Ingresso.find(item.id.to_i)
                        ingresso.update_attributes(:transacao => transaction.code, :transacao_status => status)
                    end
                end
                render nothing: true, status: 200
                return
            end
        
        render nothing: true, status: 404
        end
   end


end

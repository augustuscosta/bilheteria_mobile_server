  var map;
  var geocoder;
  var marker;
  var initialLocation;
  var browserSupportFlag =  new Boolean();

	jQuery(document).ready(function(){
   		initialize();

		jQuery('#geocodingButton').click(function(event){
		     	geocode();
		});

	 	jQuery('#locationTypeCheckbox').change(function(event){
		     	checkBoxChange(event);
		});
 	});
	
	
	
	
	
  function checkBoxChange(event){
		if(!jQuery('#locationTypeCheckbox').is(":checked")){
			jQuery('#conteudo_latitude').val(0);
			jQuery('#conteudo_longitude').val(0);
			jQuery('#containerLocation').hide();

		}else{
			if(jQuery("#conteudo_latitude").val() == 0 && jQuery("#conteudo_longitude").val() == 0){
				if(marker){
					jQuery("#conteudo_latitude").val(marker.getPosition().lat());
					jQuery("#conteudo_longitude").val(marker.getPosition().lng());
				}
				jQuery("#containerLocation").show();
				resizeMap();
			}
		}
  }
 
  function initialize() {
	
    var myOptions = {
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    geocoder = new google.maps.Geocoder();
	google.maps.event.addListener(map, 'dblclick', function(event) {
	    addMarker(event.latLng);
		jQuery("#conteudo_latitude").val(event.latLng.lat());
		jQuery("#conteudo_longitude").val(event.latLng.lng());
		geocodePosition(event.latLng);
	});
	
	findInitialMapPosition();
	
  }

  function findInitialMapPosition(){
	if((jQuery("#conteudo_latitude") != null && jQuery("#conteudo_latitude").val() != 0 && jQuery("#conteudo_latitude").val() != "") && 
		(jQuery("#conteudo_longitude") != null && jQuery("#conteudo_longitude").val() != 0 && jQuery("#conteudo_longitude").val() != "")){
		var latlng = new google.maps.LatLng(jQuery("#conteudo_latitude").val(),jQuery("#conteudo_longitude").val());
		addMarker(latlng);
		mapShow(true);
	}else{
		findUserLocation();
		mapShow(false);
	}
}

function mapShow(show){
	if(show){
		jQuery('#locationTypeCheckbox').attr('checked', true);
		jQuery("#containerLocation").show();
	}else{
		jQuery('#locationTypeCheckbox').attr('checked', false);
		jQuery("#containerLocation").hide();
	}
	resizeMap();
}

function resizeMap(){
	google.maps.event.trigger(map, 'resize');
}


  function geocode() {
    var address = jQuery("#conteudo_endereco").val();
    geocoder.geocode({
      'address': address,
      'partialmatch': true}, geocodeResult);
  }

  function geocodeResult(results, status) {
    if (status == 'OK' && results.length > 0) {
      var latLng = results[0].geometry.location;
	  addMarker(latLng);
	  jQuery("#conteudo_latitude").val(latLng.lat());
	  jQuery("#conteudo_longitude").val(latLng.lng());
	  map.fitBounds(results[0].geometry.viewport);
    } else {
      alert("Endereço não encontrado");
    }
  }

function addMarker(latLng){
	if(marker){
		marker.setMap(null);
	}
	  marker = new google.maps.Marker({
	    position: latLng,
	    map: map,
	    draggable: true
	  });
	  map.center = latLng;
	  google.maps.event.addListener(marker, 'dragend', function() {
	      jQuery("#conteudo_latitude").val(marker.getPosition().lat());
		  jQuery("#conteudo_longitude").val(marker.getPosition().lng());
		  geocodePosition(marker.getPosition());
	  });
	  geocodePosition(latLng);
}

function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
    } else {
      updateMarkerAddress('');
    }
  });
}

function updateMarkerAddress(str) {
  jQuery('#conteudo_endereco').val(str);
}

function findUserLocation(){
	// Try W3C Geolocation (Preferred)
	  if(navigator.geolocation) {
	    browserSupportFlag = true;
	    navigator.geolocation.getCurrentPosition(function(position) {
	      initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
	      map.setCenter(initialLocation);
	    }, function() {
	      handleNoGeolocation(browserSupportFlag);
	    });
	  // Try Google Gears Geolocation
	  } else if (google.gears) {
	    browserSupportFlag = true;
	    var geo = google.gears.factory.create('beta.geolocation');
	    geo.getCurrentPosition(function(position) {
	      initialLocation = new google.maps.LatLng(position.latitude,position.longitude);
	      map.setCenter(initialLocation);
	    }, function() {
	      handleNoGeoLocation(browserSupportFlag);
	    });
	  // Browser doesn't support Geolocation
	  } else {
	    browserSupportFlag = false;
	    handleNoGeolocation(browserSupportFlag);
	  }

	  function handleNoGeolocation(errorFlag) {
	    	map.setCenter(new google.maps.LatLng(0,0));
			map.setZoom(2);
	  }
}
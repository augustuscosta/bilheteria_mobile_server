Vitrine::Application.routes.draw do
  
  resources :ingressos_dashboards
  
  resources :acessos
  
  resources :notification
  
  resources :usuarios

  root :to => 'empresas#index'
  
  match 'empresas/conteudo/all' => 'Empresas#get_conteudos'

  resources :empresas do
    resources :conteudos
  end

  resources :empresas do
    resources :tipo_de_ingressos
  end
  
  get "empresas/index"

  resources :sessions


  resources :ingressos
  get "ingressos/index"
  
  match 'ingressos/conteudo/:conteudo_id' => 'Ingressos#get_ingressos_by_conteudo'
  match 'empresas/:empresa_id/conteudos/newvideo/new' => 'Conteudos#newt'
  match 'empresas/:empresa_id/conteudos/newpic/new' => 'Conteudos#newp'

end

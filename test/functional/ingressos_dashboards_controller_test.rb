require 'test_helper'

class IngressosDashboardsControllerTest < ActionController::TestCase
  setup do
    @ingressos_dashboard = ingressos_dashboards(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ingressos_dashboards)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ingressos_dashboard" do
    assert_difference('IngressosDashboard.count') do
      post :create, ingressos_dashboard: {  }
    end

    assert_redirected_to ingressos_dashboard_path(assigns(:ingressos_dashboard))
  end

  test "should show ingressos_dashboard" do
    get :show, id: @ingressos_dashboard
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ingressos_dashboard
    assert_response :success
  end

  test "should update ingressos_dashboard" do
    put :update, id: @ingressos_dashboard, ingressos_dashboard: {  }
    assert_redirected_to ingressos_dashboard_path(assigns(:ingressos_dashboard))
  end

  test "should destroy ingressos_dashboard" do
    assert_difference('IngressosDashboard.count', -1) do
      delete :destroy, id: @ingressos_dashboard
    end

    assert_redirected_to ingressos_dashboards_path
  end
end
